import 'package:customerapp/Theme/ThemeData.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


class StudentLinkDropDown extends StatefulWidget{

  String dropdownvalue;
  var items=[];
  var hint;
  var onChanges;

  StudentLinkDropDown({this.dropdownvalue,this.items,this.hint,this.onChanges});

  @override
  _StudentLinkDropDownState createState() => _StudentLinkDropDownState();
}

class _StudentLinkDropDownState extends State<StudentLinkDropDown> {

  @override
  Widget build(BuildContext context) {
   return  Card(
     shape: RoundedRectangleBorder(
       borderRadius: BorderRadius.circular(10.0),
     ),
     elevation: 0.5,
     child:  Container(
       padding: EdgeInsets.only(left: 20,right: 20,top: 2,bottom: 2),
       child:  DropdownButtonFormField(
         hint: Text(widget.hint,style: GoogleFonts.roboto(
           textStyle: TextStyle(
             letterSpacing: .5,
             fontWeight: FontWeight.w500,
             color: ShoopeesTheme.primaryColorDark,
             fontSize: 15,
           ),
         ),),
         decoration: InputDecoration(
             border: InputBorder.none
         ),
         value: widget.dropdownvalue!=null?widget.dropdownvalue:null,
         icon: const Icon(Icons.keyboard_arrow_down),
         items: widget.items.map((items) {
           return DropdownMenuItem(
             value: items,
             child: Text(items),
           );
         }).toList(),
         onChanged: (newValue) {

            widget.dropdownvalue = newValue;
             widget.onChanges(newValue);

         },
       ),
     ),
   );
  }
}