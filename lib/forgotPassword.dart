import 'package:customerapp/Ui/LogingScreen.dart';
import 'package:customerapp/repo/resetRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Ui/RegisterScreen.dart';
import 'Theme/ThemeData.dart';

class ForgotScreen extends StatelessWidget{
  TextEditingController ControllerEmail=TextEditingController();
  @override
  Widget build(BuildContext context) {
    var width=MediaQuery.of(context).size.width;
    var height=MediaQuery.of(context).size.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor:Colors.white,
      appBar: AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios_rounded,
            color: ShoopeesTheme.primaryColorDark,),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        title: ShoopeesTheme.headline1('Email to reset password',
            ShoopeesTheme.primaryColorDark,FontWeight.w500),
      ),
      body: Container(
        width: width,
        height: height,
        child:   Stack(
          children: <Widget>[


            Container(
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.all(15),
              child: Container(
                width:width,
                //height: 450,
                decoration: BoxDecoration(
                    color:ShoopeesTheme.lightlogingBg,
                    borderRadius: BorderRadius.circular(20)
                ),

                child: Container(
                  padding: EdgeInsets.all(20),
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      //logo
                      Image.asset('assets/logo/logo2.png', ),


                      //email
                      Container(
                        padding: EdgeInsets.only(left: 10,right: 10,top: 10),
                        child: TextField(
                          controller: ControllerEmail,
                          textInputAction: TextInputAction.next,
                          autocorrect: true,
                          style: ShoopeesTheme.hint,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.email_outlined),
                              hintStyle: ShoopeesTheme.hint,
                              hintText: 'Email'
                          ),
                        ),
                      ),


                      //button
                      Container(
                        margin: EdgeInsets.only(top: 45,left: 15,right: 15),
                        child: InkWell(
                          onTap: (){
                            String email=ControllerEmail.text;

                            if(email.isEmpty){
                              ShoopeesTheme.showToasty(text: 'Enter your email');

                            }else{
                              ShoopeesTheme.showToasty(text: 'Reset link sent');
                              resetRepo(ControllerEmail.text,).then((value){
                                print('valuevaluevalue==$value');

                                if(value=='Succsess'){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) =>  LogingScreen()),
                                  );
                                }else{

                                }
                              });
                            }


                          },
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            color: ShoopeesTheme.primaryColor,
                            elevation: 0,
                            child: Container(
                              width: width,
                              padding: EdgeInsets.all(15),
                              child:Center(child: ShoopeesTheme.headline1('Send link',Colors.white,FontWeight.w500)),
                            ),
                          ),
                        ),
                      ),

                      //text
                      InkWell(
                        onTap: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) =>  LogingScreen()),
                          );
                        },
                        child: Container(
                            margin: EdgeInsets.only(top: 20,left: 20,right: 20),
                            child: ShoopeesTheme.headline2("Return to login  ",Colors.black,FontWeight.normal)),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10,left: 20,right: 20,bottom: 20),
                        width: width,
                        child:Row(
                          children: [
                            ShoopeesTheme.headline2("Don't have an account?  ",Colors.black,FontWeight.normal),

                            InkWell(
                                onTap: (){
                                  print('dasfdsafadsfsdf');
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) =>  RegisterScreen()),
                                  );
                                },
                                child: ShoopeesTheme.headline2("Sign up",ShoopeesTheme.primaryColor,FontWeight.normal)
                            )
                          ],
                        ) ,
                      )


                    ],
                  ),
                ),
              ),
            ),
            ShoopeesTheme().rainbow(context),
          ],
        ),
      ),
    );
  }

}