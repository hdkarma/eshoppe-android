import 'dart:convert';
import 'dart:io';

import 'package:customerapp/Theme/ThemeData.dart';

import 'RestApi.dart';
import 'package:http/http.dart'as http;

Future resetRepo(email) async {
  String url=api_base_url+'send_reset_link_email';
  final response = await http.post(
      Uri.parse(url),
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      body: {
        'email':email
      });




  if(response.body!=null){
    ShoopeesTheme.showToasty(text: 'Reset link sent');

    return 'Success';
  }else{
    ShoopeesTheme.showToasty(text: 'Check your internet connection');
    return 'Success';
  }
}