
import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

final String DynamicLink = 'https://customerapps.page.link';

FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;
Future createDynamicLink() async {
  final DynamicLinkParameters parameters = DynamicLinkParameters(
    uriPrefix: 'https://customerapps.page.link',
    link: Uri.parse(DynamicLink),
    androidParameters:  AndroidParameters(
      packageName: 'com.eshopees.customerapp',
      minimumVersion: 0,
    ),
    iosParameters:  IOSParameters(
      bundleId: 'com.eshopees.customerapp',
      minimumVersion: '0',
    ),

  );

  Uri url;
  final ShortDynamicLink shortLink = await dynamicLinks.buildShortLink(parameters);
  url = shortLink.shortUrl;
  log('url====${ url.toString()}');
  return url.toString();
}