
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:customerapp/Theme/ThemeData.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/user.dart';
import 'RestApi.dart';
import 'package:http/http.dart'as http;


Future loginrepo({@required email,@required pass}) async {
  String url=api_base_url+login_url;
  User user=User();
  user.name='';
  user.email=email;
  user.auth=false;
  user.password=pass;
  user.apiToken='';
  user.deviceToken='token';
  user.phone='number';
  user.verificationId='verificationId';
  user.address='';
  user.bio='';
  user.id='id';
  user.verifiedPhone=true;
  final response = await http.post(
      Uri.parse(url),
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      body: json.encode(user.toMap()));


    log(response.body);
    if(response.body!=''){

      var body=[];
      body.add(jsonDecode(response.body));
      log('response====${body[0]['data']['custom_fields']['phone']['value']}');
      var name=body[0]['data']['name'];
      var email=body[0]['data']['email'];
      var number=body[0]['data']['custom_fields']['phone']['value'].toString();
      var id=body[0]['data']['id'].toString();
      final prefs = await SharedPreferences.getInstance();
      await prefs.setString('user', id.toString());
      await prefs.setString('name', name);
      await prefs.setString('email', email);
      await prefs.setString('number', number);

      ShoopeesTheme.showToasty(text: 'Login Successfull');

      FirebaseFirestore db=FirebaseFirestore.instance;

      db.collection('Refferr').doc(id).set(
          {
            'name':name,
            'email':email,
            'number':number,
            'uid':id,
            'reffered':0
          }
      ).then((value) {
        print('added00');

      });

      return 'Succsess';
    }else{
      print('body null');
      ShoopeesTheme.showToasty(text: 'Enter a valid email/password');
      return 'Faild';
    }

}

