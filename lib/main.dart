import 'package:customerapp/Theme/ThemeData.dart';
import 'package:customerapp/Ui/LogingScreen.dart';
import 'package:customerapp/Ui/MainScreen.dart';
import 'package:customerapp/Ui/RegisterScreen.dart';
import 'package:customerapp/Ui/garb/GrabScreen.dart';
import 'package:customerapp/Ui/mainscreens/HomeScreen.dart';
import 'package:customerapp/Ui/reafferal/refferalScreen.dart';
import 'package:customerapp/test.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'Ui/SplashScreen.dart';
import 'forgotPassword.dart';

 main() async {
   WidgetsFlutterBinding.ensureInitialized();
   await Firebase.initializeApp();
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      primaryColor: ShoopeesTheme.primaryColor,
      primaryColorLight: ShoopeesTheme.primaryColorLight, // Color(0xFF66d5ff),
      primaryColorDark: ShoopeesTheme.primaryColor,
    ),
    home: SplashScreen(),

  ));
}