import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Progress extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    var width=MediaQuery.of(context).size.width;
    var height=MediaQuery.of(context).size.height;
    return Container(
      margin: EdgeInsets.only(top: 10),
      width: width,
      height: 10,
      decoration: BoxDecoration(
          color: Colors.white30,
          borderRadius: BorderRadius.circular(15)
      ),
      child: Text('',style:TextStyle(),)
    );
  }

}