import 'dart:async';
import 'dart:async';
import 'dart:developer';
import 'dart:async';
import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:customerapp/Ui/MainScreen.dart';
import 'package:customerapp/Ui/garb/donepage.dart';
import 'package:customerapp/repo/registerrepo.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:place_picker/entities/location_result.dart';
import 'package:place_picker/widgets/place_picker.dart';
import 'Ui/garb/GrabScreen.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:customerapp/Ui/LogingScreen.dart';
import 'package:customerapp/repo/loginRepo.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/material.dart';
import 'package:social_share/social_share.dart';
import 'repo/dynamicLink.dart';
import 'Theme/ThemeData.dart';
import 'Ui/MainScreen.dart';
import 'Ui/RegisterScreen.dart';
import 'Ui/garb/grabintro.dart';
import 'Ui/notificationScreen.dart';
import 'Ui/reafferal/refferalScreen.dart';

class Test extends StatefulWidget {

  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> with TickerProviderStateMixin {
  TextEditingController ControllerEmail=TextEditingController();

  TextEditingController ControllerPassword=TextEditingController();

  bool showPass=true;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor:Colors.white,
      appBar: AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios_rounded,
            color: ShoopeesTheme.primaryColorDark,),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        title: ShoopeesTheme.headline1('Sign into your account',
            ShoopeesTheme.primaryColorDark,FontWeight.w500),
      ),
      body: Container(
        width: width,
        height: height,
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.all(15),
              width:width,
              //height: 450,
              decoration: BoxDecoration(
                  color:ShoopeesTheme.lightlogingBg,
                  borderRadius: BorderRadius.circular(20)
              ),

              child: Container(
                padding: EdgeInsets.all(20),
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    //logo
                    Image.asset('assets/logo/logo2.png', ),


                    //email
                    Container(
                      padding: EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        controller: ControllerEmail,
                        textInputAction: TextInputAction.next,
                        autocorrect: true,
                        style: ShoopeesTheme.hint,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.email_outlined),
                            hintStyle: ShoopeesTheme.hint,
                            hintText: 'Email'
                        ),
                      ),
                    ),

                    //pass
                    Container(
                      padding: EdgeInsets.only(left: 10,right: 10,top: 10),
                      child: TextField(
                        controller: ControllerPassword,
                        textInputAction: TextInputAction.next,
                        autocorrect: true,
                        style: ShoopeesTheme.hint,
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: showPass,
                        decoration: InputDecoration(
                          suffix: InkWell(
                              onTap: (){
                                setState(() {
                                  showPass==false?showPass=true:showPass=false;
                                });
                              },
                              child: Icon(showPass==true?Icons.visibility_off_outlined:Icons.visibility_outlined)),
                          prefixIcon: Icon(Icons.lock_outline_rounded),
                          hintStyle: ShoopeesTheme.hint,
                          hintText: 'Password',
                        ),
                      ),
                    ),

                    //button
                    Container(
                      margin: EdgeInsets.only(top: 45,left: 15,right: 15),
                      child: InkWell(
                        onTap: (){
                          String email=ControllerEmail.text;
                          String pass=ControllerPassword.text;
                          if(email.isEmpty){
                            ShoopeesTheme.showToasty(text: 'Enter your email');

                          }else if(pass.isEmpty){
                            ShoopeesTheme.showToasty(text: 'Enter your password');
                          }else{

                            loginrepo(
                                email:ControllerEmail.text,
                                pass: ControllerPassword.text).then((value){
                              print('valuevaluevalue==$value');
                              if(value=='Succsess'){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) =>  RefferScreen()),
                                );
                              }else{

                              }
                            });
                          }


                        },
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          color: ShoopeesTheme.primaryColor,
                          elevation: 0,
                          child: Container(
                            width: width,
                            padding: EdgeInsets.all(15),
                            child:Center(child: ShoopeesTheme.headline1('Sign in',Colors.white,FontWeight.w500)),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: (){

                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(builder: (context) =>  ForgotScreen()),
                        // );
                      },
                      child: Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(top: 20,left: 20,right: 20,),
                        width: width,
                        child:ShoopeesTheme.headline2("Forgot password?  ",Colors.black,FontWeight.normal) ,
                      ),
                    ),
                    //text

                    Container(
                      margin: EdgeInsets.only(top: 20,left: 20,right: 20,),
                      width: width,
                      child:Row(
                        children: [
                          ShoopeesTheme.headline2("Don't have an account?  ",Colors.black,FontWeight.normal),

                          InkWell(
                              onTap: (){
                                print('dasfdsafadsfsdf');
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) =>  RegisterScreen()),
                                );
                              },
                              child: ShoopeesTheme.headline2("Sign up",ShoopeesTheme.primaryColor,FontWeight.normal)
                          )
                        ],
                      ) ,
                    )


                  ],
                ),
              ),
            ),
            ShoopeesTheme().rainbow(context),
          ],
        ),
      ),
    );
  }

}