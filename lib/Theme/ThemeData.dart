import 'package:customerapp/models/media.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';


class ShoopeesTheme{
  //color theme
 static Color lightlogingBg=Color(0xFFFBFAFF);
 static Color primaryColor= Color(0xFF17BEF0);
 static Color primaryColorLight= Color(0xFF74C042); // Color(0xFF66d5ff),
 static Color primaryColorDark= Color(0xFF7C8892);

 //gradient
 static Color gradientprimaryColor= Color(0xFF17BEF0);
 static Color gradientprimaryColor1= Color(0xFFEDF8FF);
 static Color gradientprimaryColor2= Color(0xFF17BEF0);
 //input hint theme
 static var hint=GoogleFonts.poppins(textStyle: TextStyle(fontSize: 14, letterSpacing: .5,));


 //toast
static void showToasty({@required text}) async {
  Fluttertoast.showToast(
      msg: text,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.white,
      textColor: ShoopeesTheme.primaryColorDark,
      fontSize: 16.0
  );

}

//footer
  Widget rainbow(context){
    return  Container(
      alignment: Alignment.bottomCenter,
      width: MediaQuery.of(context).size.width,
      child: Image.asset('assets/footerimage.png'),
    );
  }

  //taxt theme
 static Widget headline1(text,color,fontweight){
    return Text(
      text,
      textAlign:TextAlign.center ,
      style: GoogleFonts.poppins(
        textStyle: TextStyle(color: color,
            fontSize: 16,
            letterSpacing: .5,
            fontWeight:fontweight),
      ),
    );
  }

 static Widget headline2(text,color,fontweight){
   return Text(
     text,
     textAlign: TextAlign.center,
     softWrap: true,
     overflow: TextOverflow.ellipsis,
     style: GoogleFonts.poppins(
       textStyle: TextStyle(color: color,
           fontSize: 14,
           letterSpacing: .5,
           fontWeight:fontweight),
     ),
   );
 }


 static Widget headline3(text,color,fontweight){
   return Text(
     text,
     style: GoogleFonts.poppins(
       textStyle: TextStyle(color: color,
           fontSize: 12,
           letterSpacing: .5,
           fontWeight:fontweight),
     ),
   );
 }

 Widget bigtittle(text,color,fontweight){
   return Text(
     text,
     textAlign: TextAlign.center,
     style: GoogleFonts.poppins(
       textStyle: TextStyle(color: color,

           fontSize: 25,
           letterSpacing: .5,
           fontWeight:fontweight),
     ),
   );
 }



}