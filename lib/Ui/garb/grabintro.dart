import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../Theme/ThemeData.dart';
import 'GrabScreen.dart';


class GrabIntro extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    var width=MediaQuery.of(context).size.width;
    var height=MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
            color: ShoopeesTheme.primaryColorDark
        ),
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor:Colors.white,
        elevation:0 ,
      ),
      body: Container(
        width: width,
        height: height,
        child: Stack(
          children: <Widget>[

            Container(
              padding: EdgeInsets.only(top: 25,left: 15,right: 15,bottom: 5),
              child: ListView(
                //  mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      padding: EdgeInsets.only(left: 70,right: 70),
                      width: width,
                      //height: 200,
                      child: Image.asset('assets/reward.png')),

                  SizedBox(
                    height: 25,
                  ),
                  ShoopeesTheme().bigtittle(
                      'Click your way to attractive rewards...',
                      ShoopeesTheme.primaryColorDark,FontWeight.bold),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    width: width,
                    alignment: Alignment.center,
                    child: ShoopeesTheme.headline2(
                        "Now,let's get to know",
                        ShoopeesTheme.primaryColorDark,FontWeight.w700),
                  ),

                  Container(
                    child: Container(
                      width: 200,
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: 10),
                      child: InkWell(
                        onTap: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) =>  GrabScreen()),
                          );
                        },
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          color: ShoopeesTheme.primaryColor,
                          elevation: 0,
                          child: Container(
                            width: width,
                            padding: EdgeInsets.all(15),
                            child:Center(
                                child: ShoopeesTheme.headline1('Get Start',Colors.white,FontWeight.w500)),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),

            ShoopeesTheme().rainbow(context),
          ],
        ),
      ),
    );
  }

}