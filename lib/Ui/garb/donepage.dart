import 'package:customerapp/Ui/reafferal/refferalScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../Theme/ThemeData.dart';
import '../MainScreen.dart';

class DoneScreen extends StatefulWidget{
  @override
  State<DoneScreen> createState() => _DoneScreenState();
}

class _DoneScreenState extends State<DoneScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 2)).then((value) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) =>  RefferScreen()),
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    var width=MediaQuery.of(context).size.width;
    var height=MediaQuery.of(context).size.height;
   return Scaffold(
     backgroundColor: Colors.white,
     appBar: AppBar(
       iconTheme: IconThemeData(
           color: ShoopeesTheme.primaryColorDark
       ),
       leading: IconButton(
         icon: Icon(Icons.close),
         onPressed: () {
           Navigator.pop(context);
         },
       ),
       backgroundColor: Colors.white,
       elevation:0 ,
     ),
     body: Container(
       width: width,
       height: height,
       child: Stack(
         children: <Widget>[

           Center(
             child: Column(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Container(
                     width: 100,
                     height: 100,
                     child: Image.asset('assets/done.png')),

                 SizedBox(
                   height: 25,
                 ),
                 Container(
                   child: ShoopeesTheme().bigtittle(
                       'Thank you and keep your fingers crossed',
                       ShoopeesTheme.primaryColorDark,FontWeight.bold),
                 ),
               ],
             ),
           ),
           ShoopeesTheme().rainbow(context),
         ],
       ),
     ),
   );
  }
}