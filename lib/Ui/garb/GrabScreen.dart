import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:customerapp/repo/RestApi.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:place_picker/entities/location_result.dart';
import 'package:place_picker/place_picker.dart';
import 'package:place_picker/widgets/place_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:location/location.dart';
import '../../Theme/ThemeData.dart';
import 'package:geolocator/geolocator.dart';
import 'donepage.dart';

class GrabScreen extends StatefulWidget   {
  @override
  State<GrabScreen> createState() => _GrabScreenState();
}

class _GrabScreenState extends State<GrabScreen>   with TickerProviderStateMixin {
   AnimationController controller;
  int index=1;
  var progresscount=0.3;

  //firebase
  FirebaseAuth auth=FirebaseAuth.instance;
  FirebaseFirestore db=FirebaseFirestore.instance;

  String name='';
  String email='';
  String id='';
  String number='';
  String Grabid='';
  String Q1Answer;
   String Q2Answer;
  String Q3Answer;

  List<String> Q1dropDown=[];
  List<String> Q2dropDown=[];


  ValueNotifier<String> currentAddress=ValueNotifier<String>('');
  TextEditingController locationController=TextEditingController();
  bool selectedLocation=false;

  @override
  void initState() {
    getQ1Answer();
    getUser();

    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..addListener(() {
      //log('${controller.value}');
      setState(() {});
    });
    controller.repeat(reverse: true);
    super.initState();
  }


  Future<void> getUser() async {
    final prefs = await SharedPreferences.getInstance();
    name=await prefs.getString('name');
    email=(await prefs.getString('email'));
    id=(await prefs.getString('user'));
    number=(await prefs.getString('number'));

    log('name==$id');

  }

  void setData(){
    db.collection('Grab').doc(id).set({
      'name':name,
      'email':email,
      'number':number,
      'q1':Q1Answer,
      'q2':locationController.text,
      'q3':Q3Answer,
      'userId':id,
    }).then((value) {

    });
  }

  getQ1Answer(){
    FirebaseFirestore db=FirebaseFirestore.instance;
    db.collection('GrabAnswers').doc('q1').collection('answers').get().then((value) {
      var a1=value.docs.length;
      for(int i=0;i<a1;i++){
        print(value.docs[i].get('a').toString());
        setState(() {
          Q1dropDown.add(value.docs[i].get('a').toString());
        });
      }
    });

    db.collection('GrabAnswers').doc('q2').collection('answers').get().then((value) {
      var a1=value.docs.length;
      for(int i=0;i<a1;i++){
        print(value.docs[i].get('a').toString());
        setState(() {
          Q2dropDown.add(value.docs[i].get('a').toString());
        });
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    var width=MediaQuery.of(context).size.width;
    var height=MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
            color: ShoopeesTheme.primaryColorDark
        ),
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor:Colors.white,
        elevation:0 ,
      ),
      body: Container(
        width: width,
        height: height,
        child: Stack(
          children: <Widget>[

            Container(
              padding: EdgeInsets.all(15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 0),
                    width: width,
                    alignment: Alignment.centerLeft,
                    child: ShoopeesTheme.headline1('Question $index /3',
                        ShoopeesTheme.primaryColorDark,FontWeight.bold),
                  ),

                  progress(width),

                  SizedBox(
                    height: 15,
                  ),

                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Container(
                      width: width,
                      padding: EdgeInsets.all(15),
                      child: index==1?q1(width):index==2?q2(width):index==3?q3(width):Container(),
                    ),
                  ),
                ],
              ),
            ),

            ShoopeesTheme().rainbow(context),
          ],
        ),
      ),
    );
  }
  
  Widget q1(width){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          height: 35,
        ),
        Container(
          alignment: Alignment.topLeft,
          child: ShoopeesTheme.headline1(
              'How often do you shop online ?',
              ShoopeesTheme.primaryColorDark,FontWeight.bold),
        ),

        SizedBox(
          height: 25,
        ),
        Padding(
          padding: EdgeInsets.all(15),
          child:  Container(
            alignment: Alignment.topCenter,
            width: width,
            child: DropdownButton(
              value: Q1Answer,
              icon: const Icon(Icons.keyboard_arrow_down),
              items: Q1dropDown.
              map((String items) {
                return DropdownMenuItem(
                  value: items,
                  child:  ShoopeesTheme.headline2(
                      items,
                      ShoopeesTheme.primaryColorDark,FontWeight.normal),
                );
              }).toList(),
              onChanged: (newValue) {
                setState(() {
                  Q1Answer = newValue;
                });
              },
            ),
          ),
        ),

        Container(
          margin: EdgeInsets.only(left: 55,right: 55,top: 25),
          child: InkWell(
            onTap: (){
              if(index==1){

                if(Q1Answer!=null){
                  setState(() {
                    index=2;
                    progresscount=0.6;
                  });
                }else{
                  ShoopeesTheme.showToasty(text: 'Select answer');

                }

              }else if(index==2){
                setState(() {
                  index=3;
                  progresscount=0.9;
                });
              }else if(index==3){

                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>  DoneScreen()),
                );
              }
            },
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              color: ShoopeesTheme.primaryColor,
              elevation: 0,
              child: Container(
                width: width,
                padding: EdgeInsets.all(15),
                child:Center(child: ShoopeesTheme.headline1('Next',Colors.white,FontWeight.w500)),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 35,
        ),
      ],
    );
  }

  Widget q2(width){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          height: 35,
        ),
        Container(
          alignment: Alignment.topLeft,
          child: ShoopeesTheme.headline1(
              'Tell us about you location and your favourite shopping application or website ?',
              ShoopeesTheme.primaryColorDark,FontWeight.bold),
        ),

        SizedBox(
          height: 25,
        ),
        Padding(
          padding: EdgeInsets.all(15),
          child: TextField(
            controller:locationController ,
            style: ShoopeesTheme.hint,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              hintText: 'Select location',
              suffixIcon: IconButton(
                  onPressed: () {
                    showPlacePicker();
                  },
                  icon: Icon(Icons.my_location_rounded,color: ShoopeesTheme.primaryColorDark,)),
              border: OutlineInputBorder(),
            ),
          ),
        ),

        Container(
          margin: EdgeInsets.only(left: 55,right: 55,top: 25),
          child: InkWell(
            onTap: (){
              if(index==1){
                setState(() {
                  index=2;
                  progresscount=0.6;
                });
              }else if(index==2){
                if(locationController.text==null){
                  ShoopeesTheme.showToasty(text: 'Select Location');
                }else{
                  setState(() {
                    index=3;
                    progresscount=0.9;
                  });
                }

              }else if(index==3){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>  DoneScreen()),
                );
              }
            },
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              color: ShoopeesTheme.primaryColor,
              elevation: 0,
              child: Container(
                width: width,
                padding: EdgeInsets.all(15),
                child:Center(child: ShoopeesTheme.headline1('Next',Colors.white,FontWeight.w500)),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 35,
        ),
      ],
    );
  }

  Widget q3(width){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          height: 35,
        ),
        Container(
          alignment: Alignment.topLeft,
          child: ShoopeesTheme.headline1(
              'Which reward would you choose from the below options ?',
              ShoopeesTheme.primaryColorDark,FontWeight.bold),
        ),

        SizedBox(
          height: 25,
        ),
        Padding(
          padding: EdgeInsets.all(15),
          child:  Container(
            alignment: Alignment.topCenter,
            width: width,
            child: DropdownButton(
              value: Q3Answer,
              icon: const Icon(Icons.keyboard_arrow_down),
              items: Q2dropDown.
              map((String items) {
                return DropdownMenuItem(
                  value: items,
                  child:  ShoopeesTheme.headline2(
                      items,
                      ShoopeesTheme.primaryColorDark,FontWeight.normal),
                );
              }).toList(),
              onChanged: (newValue) {
                setState(() {
                  Q3Answer = newValue;
                });
              },
            ),
          ),
        ),

        Container(
          margin: EdgeInsets.only(left: 55,right: 55,top: 25),
          child: InkWell(
            onTap: (){
              if(index==1){
                setState(() {
                  index=2;
                  progresscount=0.6;
                });
              }else if(index==2){
                setState(() {
                  index=3;
                  progresscount=0.9;
                });
              }else if(index==3){
                if(Q3Answer!=null){
                  setData();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) =>  DoneScreen()),
                  );
                }else{
                  ShoopeesTheme.showToasty(text: 'Select answer');
                }

              }
            },
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              color: ShoopeesTheme.primaryColor,
              elevation: 0,
              child: Container(
                width: width,
                padding: EdgeInsets.all(15),
                child:Center(child: ShoopeesTheme.headline1('Submit',Colors.white,FontWeight.w500)),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 35,
        ),
      ],
    );
  }

  Widget progress(width){
    return Container(
      margin: EdgeInsets.only(top: 10),
      width: width,
      height: 7,
      decoration: BoxDecoration(
          color: Colors.white30,
        borderRadius: BorderRadius.circular(25)
      ),
      child: LinearProgressIndicator(
        //value: ShoopeesTheme.primaryColorLight,
        value: progresscount,
      ),
    );
  }

  void showPlacePicker() async {
    var customLocation;
    LocationResult result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) =>
            PlacePicker("AIzaSyDafPdwDcLIJk5Wd9KBEwcJKnA2mhlrtGg",
              displayLocation: customLocation,
            )));

    // Handle the result in your way
    setState(() {
      locationController.text=result.formattedAddress;
      selectedLocation=true;
    });

  }




}