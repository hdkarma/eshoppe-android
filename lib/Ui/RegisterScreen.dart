import 'dart:async';
import 'dart:developer';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:customerapp/Ui/MainScreen.dart';
import 'package:customerapp/Ui/reafferal/refferalScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:pin_code_fields/pin_code_fields.dart';


import '../Theme/ThemeData.dart';
import '../repo/registerrepo.dart';
import 'LogingScreen.dart';

class RegisterScreen extends StatefulWidget{
  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController Controller_name=TextEditingController();

  TextEditingController Controller_number=TextEditingController();

  TextEditingController Controller_email=TextEditingController();

  TextEditingController Controller_password=TextEditingController();

  String name;
  String number;
  String email;
  String password;
  String token ;
  String _verificationCode;
  String  CountryCode='+91';
  bool showPass=true;

  FirebaseAuth auth = FirebaseAuth.instance;
  ValueNotifier<int> timeOut=ValueNotifier(60);
  ValueNotifier<bool> visible=ValueNotifier(false);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getDiviceTokenId();

  }

  getDiviceTokenId() async {
    try{
      final prefs = await SharedPreferences.getInstance();
      prefs.clear();
    }catch(e){
    }
    token = await FirebaseMessaging.instance.getToken();
  }


  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor:Colors.white,
      appBar: AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios_rounded,color: Colors.black,),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        title: ShoopeesTheme.headline1('Sign into your account',Colors.black,FontWeight.w500),
      ),
      body: Container(
        width: width,
        height: height,
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(left: 15,right: 15,top: 10),
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                  color:ShoopeesTheme.lightlogingBg,
                  borderRadius: BorderRadius.circular(15)
              ),
              child: ListView(
                shrinkWrap: true,
                children: [
                  //logo
                  Image.asset('assets/logo/logo2.png',height: 100,),
                  SizedBox(
                    height: 10,
                  ),
                  //name
                  Container(
                    padding: EdgeInsets.only(left: 10,right: 10),
                    child: TextField(
                      controller:Controller_name ,
                      textInputAction: TextInputAction.next,
                      autocorrect: true,
                      style: ShoopeesTheme.hint,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.person_outline_outlined),
                          hintStyle: ShoopeesTheme.hint,
                          hintText: 'Full name'
                      ),
                    ),
                  ),



                  Row(
                    children: [
                      Flexible(
                        flex:2,
                        child: CountryPickerDropdown(
                          selectedItemBuilder: (country){
                            return Center(
                              child: ShoopeesTheme.headline3('+${country.phoneCode}',
                                  ShoopeesTheme.primaryColorDark,
                                  FontWeight.normal),
                            );
                          },

                          initialValue: 'IN',
                          itemBuilder: _buildDropdownItem,
                          dropdownColor: Colors.white,
                          //itemFilter:  ['AR', 'DE', 'GB', 'CN'],
                          priorityList:[
                            CountryPickerUtils.getCountryByIsoCode('GB'),
                            CountryPickerUtils.getCountryByIsoCode('CN'),
                          ],
                          sortComparator: (Country a, Country b) => a.isoCode.compareTo(b.isoCode),
                          onValuePicked: (Country country) {
                            print("${country.name}");
                            CountryCode=country.phoneCode.toString();
                          },

                        ),
                      ),


                      Flexible(
                        flex: 6,
                        child: Container(
                          padding: EdgeInsets.only(left: 10,right: 10,top: 10),
                          child: TextField(
                            controller:Controller_number ,
                            textInputAction: TextInputAction.next,
                            autocorrect: true,
                            style: ShoopeesTheme.hint,
                            keyboardType: TextInputType.number,
                            maxLength: 10,
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.phone_iphone_rounded ),
                                hintStyle: ShoopeesTheme.hint,
                                hintText: 'Mobile number'
                            ),
                          ),
                        ),
                      )

                    ],
                  ),


                  //email
                  Container(
                    padding: EdgeInsets.only(left: 10,right: 10,top: 10),
                    child: TextField(
                      controller:Controller_email ,
                      textInputAction: TextInputAction.next,
                      autocorrect: true,
                      style: ShoopeesTheme.hint,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.email_outlined),
                          hintStyle: ShoopeesTheme.hint,
                          hintText: 'Email'
                      ),
                    ),
                  ),

                  //pass
                  Container(
                    padding: EdgeInsets.only(left: 10,right: 10,top: 10),
                    child: TextField(
                      controller:Controller_password ,
                      textInputAction: TextInputAction.next,
                      autocorrect: true,
                      style: ShoopeesTheme.hint,
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: showPass,
                      decoration: InputDecoration(
                        suffix: InkWell(
                            onTap: (){
                              setState(() {
                                showPass==false?showPass=true:showPass=false;
                              });
                            },
                            child: Icon(showPass==true?Icons.visibility_off_outlined:Icons.visibility_outlined)),
                        prefixIcon: Icon(Icons.lock_outline_rounded),
                        hintStyle: ShoopeesTheme.hint,
                        hintText: 'Password',
                      ),
                    ),
                  ),

                  //button
                  Container(
                    margin: EdgeInsets.only(top: 45,left: 15,right: 15),
                    child: InkWell(
                      onTap: (){
                        validation(context);
                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        color: ShoopeesTheme.primaryColor,
                        elevation: 0,
                        child: Container(
                          padding: EdgeInsets.all(15),
                          child:Center(child: ShoopeesTheme.headline1('Sign up',
                              Colors.white,FontWeight.w500)),
                        ),
                      ),
                    ),
                  ),

                  //text

                  Container(
                    padding: EdgeInsets.only(top: 20,left: 20),
                    child:Row(
                      children: [
                        ShoopeesTheme.headline2("Already have an account?  ",
                            Colors.black,FontWeight.normal),

                        InkWell(
                            onTap: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) =>  LogingScreen()),
                              );
                            },
                            child: ShoopeesTheme.headline2("Sign in",ShoopeesTheme.primaryColor,FontWeight.normal)
                        )
                      ],
                    ) ,
                  )

                ],
              ),

            ),
            ShoopeesTheme().rainbow(context),
          ],
        ),
      ),
    );
  }

  showbottomSheet(context){
    Timer.periodic(Duration(seconds: 1), (Timer t) {
      if(timeOut.value==0){
        visible.value=true;
      }else{
        timeOut.value=timeOut.value-1;
      }
    });
    TextEditingController textEditingController=TextEditingController();
    return showModalBottomSheet(
        backgroundColor: Colors.white,
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(20),
            child: ListView(
              children: <Widget>[
                Container(
                  child: ShoopeesTheme.headline1("Verify mobile number",
                      ShoopeesTheme.primaryColorDark,FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: ShoopeesTheme.headline2("Enter your OTP",
                      ShoopeesTheme.primaryColorDark,FontWeight.normal),
                ),
                SizedBox(
                  height: 50,
                ),
                Container(
                  padding: EdgeInsets.only(left: 30,right: 30),
                  child: PinCodeTextField(
                    appContext: context,
                    length: 6,
                    obscureText: false,
                    animationType: AnimationType.fade,
                    pinTheme: PinTheme(
                      shape: PinCodeFieldShape.box,
                      borderRadius: BorderRadius.circular(5),
                      fieldHeight: 50,
                      fieldWidth: 40,
                      selectedFillColor: Colors.white,
                      inactiveFillColor: ShoopeesTheme.gradientprimaryColor2,
                      activeColor: Colors.white,
                      inactiveColor:Colors.white ,
                      selectedColor: Colors.white,
                      disabledColor: ShoopeesTheme.primaryColorDark,
                      activeFillColor: Colors.white,
                    ),
                    animationDuration: Duration(milliseconds: 300),

                    enableActiveFill: true,
                    controller: textEditingController,
                    onCompleted: (v) {
                      print("Completed");
                    },
                    onChanged: (value) {
                      print(value);

                    },
                    beforeTextPaste: (text) {
                      print("Allowing to paste $text");
                      return true;
                    },
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Visibility(
                  visible: visible.value==true?false:true,
                  child: ValueListenableBuilder<int>(
                    valueListenable: timeOut,
                    builder: (BuildContext context, int value, Widget child){
                      return Container(
                        child: ShoopeesTheme.headline2("We will send OTP within ${value.toString()} seconds",
                            ShoopeesTheme.primaryColorDark,FontWeight.normal),
                      );
                    },
                  ),
                ),

                SizedBox(
                  height: 2,
                ),
                ValueListenableBuilder<bool>(
                  valueListenable: visible,
                  builder: (BuildContext context, bool value, Widget child) {
                    return Visibility(
                      visible:value,
                      child: Container(
                        child: InkWell(
                          onTap: (){
                            sendOTP();
                            timeOut.value=120;
                            visible.value=true;
                          },
                          child: ShoopeesTheme.headline2("Resend OTP",
                              ShoopeesTheme.primaryColorDark,FontWeight.normal),
                        ),
                      ),
                    );
                  },
                ),

                //button
                Container(
                  margin: EdgeInsets.only(top: 45,left: 15,right: 15),
                  child: InkWell(
                    onTap: (){
                      onVeriFy(otp: textEditingController.text.toString());
                    },
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      color: ShoopeesTheme.primaryColor,
                      elevation: 0,
                      child: Container(
                        padding: EdgeInsets.all(15),
                        child:Center(child: ShoopeesTheme.headline1('Verify',
                            Colors.white,FontWeight.w500)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<void> validation(context) async {
    name=Controller_name.text;
    number=Controller_number.text;
    email=Controller_email.text;
    password=Controller_password.text;
    if(name.isEmpty){
      ShoopeesTheme.showToasty(text: 'Enter your name');

    }else if(number.isEmpty){
      ShoopeesTheme.showToasty(text: 'Enter your  number');

    }else if(email.isEmpty){
      ShoopeesTheme.showToasty(text: 'Enter your email ');

    }else if(password.isEmpty){
      ShoopeesTheme.showToasty(text: 'Enter your password');

    }else{
      try{
        if (auth.currentUser!=null){
          auth.currentUser.delete();
        }
      }catch(e){

      }

      showbottomSheet(context);
      sendOTP();
    }

    // showbottomSheet(context);

  }

  Future<void> sendOTP() async {
    var num=CountryCode+number.toString();
    print('number===$num');
    await auth.verifyPhoneNumber(
        phoneNumber: num,
        verificationCompleted: (PhoneAuthCredential credential) async {
          await auth
              .signInWithCredential(credential)
              .then((value) async {
            if (value.user != null) {
              print(value.user);

            }
          });
        },
        verificationFailed: (FirebaseAuthException e) {
          if(e.message=="We have blocked all requests from this device due to unusual activity. Try again later."){
            ShoopeesTheme.showToasty(text: 'We have blocked all requests from this device due to unusual activity. Try again later.');
          }else{
            ShoopeesTheme.showToasty(text: 'Check your internet connection');
          }
          log('Regis=Error${e.message.toString()}');

        },
        codeSent: ( verficationID,  resendToken) {
          print(verficationID);
          _verificationCode=verficationID;
          ShoopeesTheme.showToasty(text: 'OTP Sent');

        },
        codeAutoRetrievalTimeout: (String verificationID) {
          print('codeAutoRetrievalTimeout');
          ShoopeesTheme.showToasty(text: 'Timed out waiting for OTP');
          _verificationCode=verificationID;

        },
        timeout: Duration(seconds: 120));
  }

  Future<void> onVeriFy({@required String otp}) async {


    try {
      log('otpptptpt$otp');
      await auth
          .signInWithCredential(PhoneAuthProvider.credential(
          verificationId: _verificationCode, smsCode: otp))
          .then((value) async {
        if (value.credential.token != null) {
          String uid=value.user.uid;
          registerUser(
              name: name,
              number: number,
              email: email,
              password: password,
              token:token,
              id: uid.toString(),
              verifiedPhone:true,
              verificationId:_verificationCode
          ).then((value) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) =>  RefferScreen()),
            );
          });
        }
      });
    } catch (e) {
      log('otpptptpt$e');
      if(e=='The sms code has expired. Please re-send the verification code to try again') {
        ShoopeesTheme.showToasty(
            text: 'The sms code has expired. Please re-send the verification code to try again');
      }else if(auth.currentUser.uid!=null){
        String uid=auth.currentUser.uid.toString();
        registerUser(
            name: name,
            number: number,
            email: email,
            password: password,
            token:token,
            id: uid.toString(),
            verifiedPhone:true,
            verificationId:_verificationCode
        ).then((value) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) =>  RefferScreen()),
          );
        });
      }

      else{
        ShoopeesTheme.showToasty(text: 'invalid OTP');
      }


    }
  }

  Widget _buildDropdownItem(Country country) => Container(
    child: Container(
      child: Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(
            width: 8.0,
          ),
          Text("+${country.phoneCode}"),
        ],
      ),
    ),
  );
}