import 'package:customerapp/Theme/ThemeData.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'LogingScreen.dart';


class IntroScreen extends StatefulWidget {
  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  int page=0;
  @override
  Widget build(BuildContext context) {
    var width=MediaQuery.of(context).size.width;
    var height=MediaQuery.of(context).size.height;
   return  Scaffold(
     backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          width: width,
          height: height,
          child: Column(
            children: [
              //dots
              Container(
                padding: EdgeInsets.only(right: 15),
                width:width,
                height: height*0.1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    if(page==0)
                      circleBar(true)
                    else
                      circleBar(false),

                    if(page==1)
                      circleBar(true)
                    else
                      circleBar(false),


                    if(page==2)
                      circleBar(true)
                    else
                      circleBar(false),
                  ],
                ),
              ),

              Container(
                height: height*0.7,
                child: PageView(
                  onPageChanged: (index){
                    setState(() {
                      page=index;
                    });


                  },
                  children: [
                    //inrto1
                    Container(
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 50),
                            width: width,
                            child: Center(
                                child: ShoopeesTheme().bigtittle("Easy and convenient",
                                    ShoopeesTheme.primaryColor,FontWeight.bold)),
                          ),

                          //des
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            width: width,
                            child: Center(
                                child: ShoopeesTheme.headline1("sit back,relax & order",
                                    Colors.black12,FontWeight.bold)),
                          ),
                          Image.asset('assets/intro/intro1.png')
                        ],
                      ),
                    ),

                    //inrto2
                    Container(
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 50),
                            width: width,
                            child: Center(
                                child: ShoopeesTheme().bigtittle("Smooth payment\nwith COD option",
                                    ShoopeesTheme.primaryColor,FontWeight.bold)),
                          ),

                          //des
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            width: width,
                            child: Center(
                                child: ShoopeesTheme.headline1("pay the way you like, enjoy",
                                    Colors.black12,FontWeight.bold)),
                          ),
                          Image.asset('assets/intro/intro2.png')
                        ],
                      ),
                    ),


                    //inrto3
                    Container(
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 50),
                            width: width,
                            child: Center(
                                child: ShoopeesTheme.headline1("Easy tarcking",
                                    ShoopeesTheme.primaryColor,FontWeight.bold)),
                          ),

                          //des
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            width: width,
                            child: Center(
                                child: ShoopeesTheme().bigtittle("track order with pin point location",
                                    Colors.black12,FontWeight.bold)),
                          ),
                          Image.asset('assets/intro/intro3.png')
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              Container(
                margin: EdgeInsets.only(left: 15,right: 15),
                child: InkWell(
                  onTap: (){
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) =>  LogingScreen()),
                    );
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    color: ShoopeesTheme.primaryColor,
                    elevation: 0,
                    child: Container(
                      width: width,
                      padding: EdgeInsets.all(15),
                      child:Center(child: ShoopeesTheme.headline1('Get Started',Colors.white,FontWeight.w500)),
                    ),
                  ),
                ),
              ),



              //
            ],
          ),
        ),
      )
    );
  }

  Widget circleBar(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 50),
      margin: EdgeInsets.symmetric(horizontal: 8),
      height: isActive ? 8 : 8,
      width: isActive ? 23 : 10,
      decoration: BoxDecoration(color: isActive ? ShoopeesTheme.primaryColor : Colors.grey, borderRadius: BorderRadius.all(Radius.circular(12))),
    );
  }
}