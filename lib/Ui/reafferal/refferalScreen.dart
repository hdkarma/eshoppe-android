import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:customerapp/Ui/garb/grabintro.dart';
import 'package:customerapp/Ui/notificationScreen.dart';
import 'package:customerapp/repo/RestApi.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_share/social_share.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../Theme/ThemeData.dart';
import '../../repo/dynamicLink.dart';
import '../LogingScreen.dart';

class RefferScreen extends StatefulWidget{
  @override
  State<RefferScreen> createState() => _RefferScreenState();
}

class _RefferScreenState extends State<RefferScreen> {
  String  url;
  FirebaseFirestore db=FirebaseFirestore.instance;
  FirebaseAuth auth=FirebaseAuth.instance;
   int count=0;
  bool click=true;
  String name='';
  String email='';
  String id='';
  String number='';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();
    grabChecking();
    createDynamicLink().then((value) {
      if(count==10){
        setState(() {
          click=false;
          url='https://eshopees.page.link';
        });
      }else{
        setState(() {
          url=value;
        });
      }

    });

  }

  void setLimit({count}){

    db.collection('Refferr').doc(id).set(
        {
          'name':name,
          'email':email,
          'number':number,
          'uid':id,
          'reffered':count
        }
    ).then((value) {
    print('added00');
    getLimit();
    });
  }

  Future<void> getUser() async {
    final prefs = await SharedPreferences.getInstance();
    name=await prefs.getString('name');
    email=(await prefs.getString('email'));
    id=(await prefs.getString('user'));
    number=(await prefs.getString('number'));


    getLimit();
  }

  void getLimit(){

    db.collection('Refferr').doc(id).get().then((value) {
      log(value.get('reffered').toString());
      setState(() {
        count=value.get('reffered');
      });

    });
  }

  Widget drawer(context){
    setState(() {
      getUser();
    });

    return Drawer(
      //backgroundColor: Colors.white,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Color(0x197C8892),
            ),
            child: Stack(
              children: [
                Column(
                  //mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    Container(
                      child: Image.asset('assets/profile.png'),
                      width: 100,
                    ),
                    ShoopeesTheme.headline1('${name=='null'?'loading...':name}', ShoopeesTheme.primaryColor,
                        FontWeight.w700),

                    ShoopeesTheme.headline2('${email=='null'?'loading...':email}', ShoopeesTheme.primaryColorDark,
                        FontWeight.normal),

                  ],
                ),
              ],
            ),
          ),

          ListTile(
            leading: Icon(Icons.home),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Home', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () {
            },
          ),


          ListTile(
            leading: Icon(Icons.notifications),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Notification', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>  NotificationScreen()),
              );
            },
          ),

          SizedBox(
            height: 5,
          ),
          ListTile(
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Application Preferences', ShoopeesTheme.primaryColorDark,
                  FontWeight.w500),
            ),
          ),

          ListTile(
            leading: Icon(Icons.help_outline),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Help & Support', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () {
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(builder: (context) =>  NotificationScreen()),
              // );
            },
          ),

          ListTile(
            leading: Icon(Icons.help_outline),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Log out', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () async {
              final prefs = await SharedPreferences.getInstance();
              prefs.remove('user');
              Future.delayed(Duration(seconds: 2)).then((value) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) =>  LogingScreen()),
                );
              });
            },
          ),
        ],
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    var width=MediaQuery.of(context).size.width;
    var height=MediaQuery.of(context).size.height;
    return Scaffold(
      drawer: drawer(context),
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
            color: ShoopeesTheme.primaryColorDark
        ),
        backgroundColor:Colors.white,
        elevation:0 ,
      ),
      body: Container(
        width: width,
        height: height,
        child: Stack(
          children: <Widget>[

            Container(
              width: width,
              height: height,
              child: Container(
                padding: EdgeInsets.all(10),
                child: ListView(
                  children: [
                    Container(
                        width: 100,
                        height: 100,
                        child: Image.asset('assets/logo/logo2.png')
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    ShoopeesTheme().bigtittle(
                        'Higher the referrals better the rewards*',
                        ShoopeesTheme.primaryColorDark,FontWeight.bold),



                    SizedBox(
                      height: 25,
                    ),

                    Container(
                        padding: EdgeInsets.only(left: 60,right: 60),
                        width: width,
                        //height: 200,
                        child: Image.asset('assets/graboffq.png'),),



                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10,right: 10,top: 5),
                      alignment: Alignment.center,
                      child: ShoopeesTheme.headline1(
                          '*Limited period entries for successful referrals',
                          ShoopeesTheme.primaryColorDark,FontWeight.w500),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 30,right: 30),
                      width: width,
                      decoration: BoxDecoration(
                        color: ShoopeesTheme.gradientprimaryColor2,
                        borderRadius: BorderRadius.all(
                            Radius.circular(10) //
                        ),

                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.all(15),
                            child: Text(
                             // '',
                              '${url!=null?url:'Loading ...'}',
                              overflow: TextOverflow.ellipsis,
                              softWrap: true,
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(color: ShoopeesTheme.primaryColorDark,
                                    fontSize: 12,
                                    letterSpacing: .5,
                                    fontWeight:FontWeight.w500),
                              ),
                            ),
                          ),
                          IconButton(
                            onPressed: () async {
                              if(count==10){
                                ShoopeesTheme.showToasty(text: 'Your limit i.'
                                    ''
                                    's exceeded');
                              }else{
                                setLimit(count: count+1);
                                await FlutterShare.share(
                                    title: 'eShopees',
                                    text: "Higher the better\n\n"
                                    "Get rewarded by sharing eShopees with your friends and contacts.\n"
                                        "\n\n"
                                        "Here’s how it works:\n"
                                        "step 1:Download eshopees application and register with us\n"
                                        "step 2:Refer your friends\n"
                                        "step 3:Share and Earn Rewards",
                                    linkUrl: '$url',
                                    chooserTitle: 'Refer you friends'
                                );
                              }

                            },

                            icon: Icon(Icons.share),
                          )
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 25,
                    ),




                    Visibility(
                      visible: Isgrab,
                      child: InkWell(
                        onTap: (){
                          grabChecking();
                          setState(() {

                          });
                          if(Isgrab==true){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>  GrabIntro()),
                            );
                          }

                        },
                        child: Container(
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ShoopeesTheme.headline2(
                                  'Grab your rewards',
                                  ShoopeesTheme.primaryColorDark,FontWeight.w500),
                              SizedBox(
                                width: 2,
                              ),
                              Icon(Icons.arrow_forward_rounded)
                            ],
                          ),
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    InkWell(
                      onTap: (){
                        _launchURL('http://eshopees.com/sell-with-us');
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ShoopeesTheme.headline3(
                                'Sell with Us',
                                ShoopeesTheme.primaryColor,FontWeight.w500),

                           // Icon(Icons.arrow_forward_rounded)
                          ],
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 15,
                    ),

                    InkWell(
                      onTap: (){
                        _launchURL('http://store.eshopees.com/register');
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ShoopeesTheme.headline3(
                                'About Us',
                                ShoopeesTheme.primaryColor,FontWeight.w500),

                            // Icon(Icons.arrow_forward_rounded)
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 82,
                    ),
                  ],
                ),
              ),
            ),
            ShoopeesTheme().rainbow(context),
          ],
        ),
      ),
    );
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}