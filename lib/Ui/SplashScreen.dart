import 'dart:developer';

import 'package:customerapp/Ui/LogingScreen.dart';
import 'package:customerapp/Ui/MainScreen.dart';
import 'package:customerapp/Ui/reafferal/refferalScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'introScreen.dart';


class SplashScreen extends StatefulWidget{
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    checkUser().then((value) {
      log(value);
      if(value==''){
        Future.delayed(Duration(seconds: 2)).then((value) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) =>  MainScreen()),
          );
        });
      }else{
        Future.delayed(Duration(seconds: 2)).then((value) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) =>  RefferScreen()),
          );
        });
      }
    });

  }

  Future checkUser() async {
    final prefs = await SharedPreferences.getInstance();
    final String user = prefs.getString('user');
    return user==null?'':user;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Container(
        child: Center(
          child: Container(
              child: Image.asset('assets/logo/logo2.png',height: 150,)),
        ),
      ),
    );
  }
}