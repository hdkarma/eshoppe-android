import 'dart:developer';

import 'package:customerapp/Ui/reafferal/refferalScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


import '../Theme/ThemeData.dart';
import 'LogingScreen.dart';
import 'MainScreen.dart';

class NotificationScreen extends StatefulWidget{
  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  String name='';

  String email='';

  @override
  void initState() {
    super.initState();
    getUser();

  }

  Future<void> getUser() async {
    final prefs = await SharedPreferences.getInstance();
    name=await prefs.getString('name');
    email=(await prefs.getString('email'));

    setState(() {

    });

    log('Notificationname==$name');
  }

  @override
  Widget build(BuildContext context) {
    var width=MediaQuery.of(context).size.width;
    var height=MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: drawer(context),
      appBar: AppBar(
        iconTheme: IconThemeData(
            color: Colors.black
        ),
        backgroundColor: Colors.white,
        elevation: 5,
        title: Container(
          // alignment: Alignment.center,
          child: ShoopeesTheme.headline1('Notifications',
              ShoopeesTheme.primaryColorDark,FontWeight.w600),
        ),
      ),
      body: Container(
        width: width,
        height: height,
        child: Stack(
          children: <Widget>[

            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Image.asset('assets/nodata.png'),
                    height: 250,
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 0),
                    child: ShoopeesTheme.headline1("Don't have any item in the list",
                        ShoopeesTheme.primaryColorDark,FontWeight.normal),
                  ),

                  Container(
                    margin: EdgeInsets.only(left: 55,right: 55,top: 2),
                    child: InkWell(
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>  RefferScreen()),
                        );
                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        color: ShoopeesTheme.primaryColor,
                        elevation: 0,
                        child: Container(
                          width: width,
                          padding: EdgeInsets.all(15),
                          child:Center(child: ShoopeesTheme.headline1('Start Exploring',Colors.white,FontWeight.w500)),
                        ),
                      ),
                    ),
                  ),

                ],
              ),
            ),
            ShoopeesTheme().rainbow(context),
          ],
        ),
      ),
    );
  }

  Widget drawer(context){
    return Drawer(
     // backgroundColor: Colors.white,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Color(0x197C8892),
            ),
            child: Stack(
              children: [
                Column(
                  //mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    Container(
                      child: Image.asset('assets/profile.png'),
                      width: 100,
                    ),
                    ShoopeesTheme.headline1('$name', ShoopeesTheme.primaryColor,
                        FontWeight.w700),

                    ShoopeesTheme.headline2('$email', ShoopeesTheme.primaryColorDark,
                        FontWeight.normal),

                  ],
                ),
              ],
            ),
          ),

          ListTile(
            leading: Icon(Icons.home),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Home', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>  RefferScreen()),
              );
            },
          ),


          ListTile(
            leading: Icon(Icons.notifications),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Notification', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>  NotificationScreen()),
              );
            },
          ),

          SizedBox(
            height: 5,
          ),
          ListTile(
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Application Preferences', ShoopeesTheme.primaryColorDark,
                  FontWeight.w500),
            ),
          ),

          ListTile(
            leading: Icon(Icons.help_outline),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Help & Support', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () {
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(builder: (context) =>  NotificationScreen()),
              // );
            },
          ),

          ListTile(
            leading: Icon(Icons.help_outline),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Log out', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () async {
              final prefs = await SharedPreferences.getInstance();
              prefs.remove('user');
              Future.delayed(Duration(seconds: 2)).then((value) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) =>  LogingScreen()),
                );
              });
            },
          ),
        ],
      ),
    );
  }
}