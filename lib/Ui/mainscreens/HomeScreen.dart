import 'dart:async';
import 'dart:developer';
import 'package:date_count_down/date_count_down.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:customerapp/Ui/LogingScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';

import '../../Theme/ThemeData.dart';
import '../RegisterScreen.dart';
import '../garb/grabintro.dart';
import '../notificationScreen.dart';
import '../reafferal/refferalScreen.dart';

class HomeScreen extends StatefulWidget{

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var endDate=DateTime(2022,05,20);

  var hour=0.0;

  var day=0.0;

  var minut=0.0;

  var second=60;

  String name='';

  String email='';
  int endTime = 1653021720000;


  @override
  void initState() {
    super.initState();
    getUser();

  }

  Future<void> getUser() async {
    final prefs = await SharedPreferences.getInstance();
    name=await prefs.getString('name');
    email=(await prefs.getString('email'));

    log('name==$name');
  }

  PreferredSizeWidget appBar(context){
    return AppBar(
      iconTheme: IconThemeData(
          color: ShoopeesTheme.primaryColorDark
      ),
      actions: [

      ],
      elevation: 0,
      backgroundColor: Colors.white,
    );
  }

  Widget drawer(context){
    return Drawer(
      //backgroundColor: Colors.white,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Color(0x197C8892),
            ),
            child: Stack(
              children: [
                Column(
                  //mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    Container(
                      child: Image.asset('assets/profile.png'),
                      width: 100,
                    ),
                    ShoopeesTheme.headline1('$name', ShoopeesTheme.primaryColor,
                        FontWeight.w700),

                    ShoopeesTheme.headline2('$email', ShoopeesTheme.primaryColorDark,
                        FontWeight.normal),

                  ],
                ),
              ],
            ),
          ),

          ListTile(
            leading: Icon(Icons.home),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Home', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () {
            },
          ),


          ListTile(
            leading: Icon(Icons.notifications),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Notification', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>  NotificationScreen()),
              );
            },
          ),

          SizedBox(
            height: 5,
          ),
          ListTile(
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Application Preferences', ShoopeesTheme.primaryColorDark,
                  FontWeight.w500),
            ),
          ),

          ListTile(
            leading: Icon(Icons.help_outline),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Help & Support', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () {
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(builder: (context) =>  NotificationScreen()),
              // );
            },
          ),

          ListTile(
            leading: Icon(Icons.help_outline),
            title:  Align(
              alignment: Alignment.centerLeft,
              child: ShoopeesTheme.headline2('Log out', ShoopeesTheme.primaryColor,
                  FontWeight.w800),
            ),
            onTap: () async {
              final prefs = await SharedPreferences.getInstance();
              prefs.remove('user');
              Future.delayed(Duration(seconds: 2)).then((value) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) =>  LogingScreen()),
                );
              });
            },
          ),
        ],
      ),
    );
  }

  Widget counter(context){
    return  //counter
      CountdownTimer(
      endTime: endTime,
      widgetBuilder: (_,  time) {
        if (time == null) {
          return Text('Game over');
        }
        return Container(
          padding: EdgeInsets.only(left: 20,right: 20,bottom: 10,top: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //day
              Container(
                alignment: Alignment.centerLeft,
                child: ShoopeesTheme().bigtittle('${time.days}',
                    ShoopeesTheme.primaryColorDark,FontWeight.w600),
              ),

              Container(
                margin: EdgeInsets.only(top: 7),
                alignment: Alignment.centerLeft,
                child: ShoopeesTheme.headline2('days',
                    ShoopeesTheme.primaryColorDark,FontWeight.normal),
              ),


              //hour
              Container(
                margin: EdgeInsets.only(left: 20),
                alignment: Alignment.centerLeft,
                child: ShoopeesTheme().bigtittle('${time.hours}',
                    ShoopeesTheme.primaryColorDark,FontWeight.w600),
              ),

              Container(
                margin: EdgeInsets.only(top: 7),
                alignment: Alignment.centerLeft,
                child: ShoopeesTheme.headline2('hrs',
                    ShoopeesTheme.primaryColorDark,FontWeight.normal),
              ),


              //minute
              Container(
                margin: EdgeInsets.only(left: 20),
                alignment: Alignment.centerLeft,
                child: ShoopeesTheme().bigtittle('${time.min}',
                    ShoopeesTheme.primaryColorDark,FontWeight.w600),
              ),

              Container(
                margin: EdgeInsets.only(top:7),
                alignment: Alignment.centerLeft,
                child: ShoopeesTheme.headline2('min',
                    ShoopeesTheme.primaryColorDark,FontWeight.normal),
              ),


              //minute
              Container(
                margin: EdgeInsets.only(left: 20),
                alignment: Alignment.centerLeft,
                child: ShoopeesTheme().bigtittle('${time.sec}',
                    ShoopeesTheme.primaryColorDark,FontWeight.w600),
              ),

              Container(
                margin: EdgeInsets.only(top:7),
                alignment: Alignment.centerLeft,
                child: ShoopeesTheme.headline2('sec',
                    ShoopeesTheme.primaryColorDark,FontWeight.normal),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget Rafer(width){
    return InkWell(
      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>  RefferScreen()),
        );
      },
      child: Card(
        color:  ShoopeesTheme.primaryColor,
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Container(
          // width: width*0.5,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: 25,
              ),
              Image.asset('assets/rafer.png',),
              SizedBox(
                height: 12,
              ),
              ShoopeesTheme.headline1('Refer your\n friends',Colors.white,
                  FontWeight.w700),
              SizedBox(
                height: 25,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Grab(width){
    return InkWell(
      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>  GrabIntro()),
        );
      },
      child: Card(
        color:  ShoopeesTheme.primaryColor,
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: 25,
              ),
              Image.asset('assets/reward.png',),
              SizedBox(
                height: 10,
              ),
              ShoopeesTheme.headline1('Grab your\n reward', Colors.white,
                  FontWeight.w700),
              SizedBox(
                height: 25,
              ),

            ],
          ),
        ),
      ),
    );
  }



  Widget subscribe(width){
    return Container(
      height: 35,
      margin: EdgeInsets.only(left: 25,right: 25),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
            Radius.circular(10) //
        ),
        border: Border.all(
          color: ShoopeesTheme.primaryColorDark, //
          width: 2.0,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            flex: 3,
            child:  Container(
              padding: EdgeInsets.only(top: 13,left: 10),
              child: TextField(
                controller: emailController,
                textInputAction: TextInputAction.next,
                autocorrect: true,
                style: ShoopeesTheme.hint,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintStyle: ShoopeesTheme.hint,
                  hintText: 'Enter email',
                ),
              ),
            ),
          ),

          Flexible(
            flex: 2,
            child: Container(
              height: 36.8,
              width: 101.5,
              decoration: BoxDecoration(
                color: ShoopeesTheme.primaryColorDark,
                borderRadius: BorderRadius.all(
                    Radius.circular(8) //
                ),
              ),
              child:InkWell(
                onTap: (){
                  addSubscribe();
                },
                child: Center(
                  child: ShoopeesTheme.headline3('Subscribe',
                      Colors.white,FontWeight.w700),
                ),
              ),

            ),
          )
        ],
      ),
    );
  }

  void addSubscribe(){

    FirebaseFirestore db=FirebaseFirestore.instance;
    db.collection('Subscribe').add({
      'email':emailController.text
    });
    setState(() {
      emailController.text='';
    });

  }

  TextEditingController emailController=TextEditingController();

  @override
  Widget build(BuildContext context) {
    var width=MediaQuery.of(context).size.width;
    var height=MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
    //  drawer: drawer(context),
      body: Container(
        margin: EdgeInsets.only(top: 25),
        width: width,
        height: height,
        child:   Container(
        //  padding: EdgeInsets.all(15),
          width: width,
          height: height,
          child: SingleChildScrollView(
            child: Column(
              children: [
                //text
                SizedBox(
                  height: 15,
                ),

                Container(
                  height: 170,
                  child: Stack(
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 15,right: 15,),
                        width: width,
                        child: Text(
                          'Welcome to the\nworld of...',
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              color: ShoopeesTheme.primaryColorDark,
                              fontSize: 25,
                              letterSpacing: .5,
                              fontWeight:FontWeight.bold,),
                          ),
                        ),
                      ),


                      Positioned(
                          left: 125,
                          top: 50,
                          child: Image.asset('assets/logo/logo2.png',)
                      ),

                    ],
                  ),
                ),







                Container(
                  padding: EdgeInsets.only(top: 15),
                  child: ShoopeesTheme.headline2('We are coming to you in',
                      ShoopeesTheme.primaryColorDark,FontWeight.w700),
                ),


                //counter
                Container(
                    padding: EdgeInsets.only(left: 15,right: 15),
                    child: counter(context)
                ),

                // //grab and reffer

                Image.asset('assets/log.png',height: 300,),
                ShoopeesTheme.headline1("Register, share your thoughts and get rewarded ",Colors.black,FontWeight.normal),
                Container(
                  margin: EdgeInsets.only(top: 20,left: 20,),
                  width: width,
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ShoopeesTheme.headline2("Don't have an account?  ",Colors.black,FontWeight.normal),

                      InkWell(
                          onTap: (){
                            print('dasfdsafadsfsdf');
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>  RegisterScreen()),
                            );
                          },
                          child: ShoopeesTheme.headline2("Sign up",ShoopeesTheme.primaryColor,FontWeight.normal)
                      )
                    ],
                  ) ,
                ),

                Container(
                  margin: EdgeInsets.only(top: 20,left: 20),
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ShoopeesTheme.headline2("Already have an account?  ",
                          Colors.black,FontWeight.normal),

                      InkWell(
                          onTap: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>  LogingScreen()),
                            );
                          },
                          child: ShoopeesTheme.headline2("Sign in",ShoopeesTheme.primaryColor,FontWeight.normal)
                      )
                    ],
                  ) ,
                ),

                //footer

                SizedBox(
                  height: 30,
                ),
                ShoopeesTheme.headline2('Get notified when we go live',
                    ShoopeesTheme.primaryColorDark,FontWeight.normal),

                SizedBox(
                  height: 10,
                ),

                Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(30, 0, 30, 0),
                  child: Container(
                    height: 40,
                    width: width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(10) //
                      ),
                      border: Border.all(
                        color: ShoopeesTheme.primaryColorDark, //
                        width: 2.0,
                      ),
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: TextFormField(
                            controller: emailController,
                            style: ShoopeesTheme.hint,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintStyle: ShoopeesTheme.hint,
                              hintText: 'Enter email',
                            ),
                           // style: FlutterFlowTheme.of(context).bodyText1,
                          ),
                        ),
                        Container(
                          height: 36.8,
                          width: 101.5,
                          decoration: BoxDecoration(
                            color: ShoopeesTheme.primaryColorDark,
                            borderRadius: BorderRadius.all(
                                Radius.circular(8) //
                            ),
                          ),
                          child:InkWell(
                            onTap: (){
                              addSubscribe();
                            },
                            child: Center(
                              child: ShoopeesTheme.headline3('Subscribe',
                                  Colors.white,FontWeight.w700),
                            ),
                          ),

                        ),
                      ],
                    ),
                  ),
                ),




                //social media
                Container(
                  margin: EdgeInsets.only(top: 10,left: 75,right: 75),
                  width: width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: Image.asset('assets/socialmedia/facebook.png'),
                        width: 25,
                        height: 25,
                      ),

                      Container(
                        child: Image.asset('assets/socialmedia/instagram.png'),
                        width: 25,
                        height: 25,
                      ),

                      Container(
                        child: Image.asset('assets/socialmedia/twitter-sign.png'),
                        width: 25,
                        height: 25,
                      ),

                      Container(
                        child: Image.asset('assets/socialmedia/youtube.png'),
                        width: 25,
                        height: 25,
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: 10,
                ),
                //text
                Container(
                  child: ShoopeesTheme.headline3('Contact us at',
                      ShoopeesTheme.primaryColorDark,FontWeight.normal),
                ),
                SizedBox(
                  height: 2,
                ),
                //text
                Center(
                  child: ShoopeesTheme.headline2('support@eshopees.com',
                      ShoopeesTheme.primaryColorDark,FontWeight.normal),
                ),

                ShoopeesTheme().rainbow(context),
                //rainbow(width)

              ],
            ),
          ),
        ),
      ),
    );
  }
}
