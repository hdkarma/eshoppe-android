
////////////////Home OG=========================
// class HomeScreen extends StatefulWidget{
//
//   @override
//   State<HomeScreen> createState() => _HomeScreenState();
// }
//
// class _HomeScreenState extends State<HomeScreen> {
//   var endDate=DateTime(2022, 04, 07 ,05,10,22);
//
//   var hour=0.0;
//
//   var day=0.0;
//
//   var minut=0.0;
//
//   var second=60;
//
//   String name='';
//   String email='';
//   void  counting(){
//     Timer.periodic(Duration(seconds: 1), (timer) {
//       setState(() {
//         final date2 = DateTime.now();
//         var millisecond = date2.difference(endDate).inSeconds;
//         minut=millisecond/60;
//         hour=minut/60;
//         day=hour/24;
//         if(second<=61){
//           second=second-1;
//
//         }
//         if(second==0){
//           second=60;
//
//
//         }
//         // double tosecond=millisecond/1000;
//         // second=millisecond.toString();
//         //  log('$millisecond');
//       });
//
//       //log('$day Day |$hour Hour |$minut Minu |$second Sec');
//     });
//
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     getUser();
//     counting();
//   }
//
//   Future<void> getUser() async {
//     final prefs = await SharedPreferences.getInstance();
//     name=await prefs.getString('name');
//     email=(await prefs.getString('email'));
//
//     log('name==$name');
//   }
//
//   PreferredSizeWidget appBar(context){
//     return AppBar(
//       iconTheme: IconThemeData(
//           color: ShoopeesTheme.primaryColorDark
//       ),
//       actions: [
//         IconButton(
//           icon: Icon(Icons.notifications_none_rounded,color: ShoopeesTheme.primaryColorDark,),
//           onPressed: (){
//             Navigator.push(
//               context,
//               MaterialPageRoute(builder: (context) =>
//                   NotificationScreen()),
//             );
//           },
//         ),
//       ],
//       elevation: 0,
//       backgroundColor: Colors.white,
//     );
//   }
//
//   Widget drawer(context){
//     return Drawer(
//       //backgroundColor: Colors.white,
//       child: ListView(
//         padding: EdgeInsets.zero,
//         children: [
//           DrawerHeader(
//             decoration: BoxDecoration(
//               color: Color(0x197C8892),
//             ),
//             child: Stack(
//              children: [
//                Column(
//                  //mainAxisAlignment: MainAxisAlignment.end,
//                  children: [
//
//                    Container(
//                      child: Image.asset('assets/profile.png'),
//                      width: 100,
//                    ),
//                    ShoopeesTheme.headline1('$name', ShoopeesTheme.primaryColor,
//                        FontWeight.w700),
//
//                    ShoopeesTheme.headline2('$email', ShoopeesTheme.primaryColorDark,
//                        FontWeight.normal),
//
//                  ],
//                ),
//              ],
//             ),
//           ),
//
//           ListTile(
//             leading: Icon(Icons.home),
//             title:  Align(
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme.headline2('Home', ShoopeesTheme.primaryColor,
//                   FontWeight.w800),
//             ),
//             onTap: () {
//             },
//           ),
//
//
//           ListTile(
//             leading: Icon(Icons.notifications),
//             title:  Align(
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme.headline2('Notification', ShoopeesTheme.primaryColor,
//                   FontWeight.w800),
//             ),
//             onTap: () {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(builder: (context) =>  NotificationScreen()),
//               );
//             },
//           ),
//
//           SizedBox(
//             height: 5,
//           ),
//           ListTile(
//             title:  Align(
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme.headline2('Application Preferences', ShoopeesTheme.primaryColorDark,
//                   FontWeight.w500),
//             ),
//           ),
//
//           ListTile(
//             leading: Icon(Icons.help_outline),
//             title:  Align(
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme.headline2('Help & Support', ShoopeesTheme.primaryColor,
//                   FontWeight.w800),
//             ),
//             onTap: () {
//               // Navigator.push(
//               //   context,
//               //   MaterialPageRoute(builder: (context) =>  NotificationScreen()),
//               // );
//             },
//           ),
//
//           ListTile(
//             leading: Icon(Icons.help_outline),
//             title:  Align(
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme.headline2('Log out', ShoopeesTheme.primaryColor,
//                   FontWeight.w800),
//             ),
//             onTap: () async {
//               final prefs = await SharedPreferences.getInstance();
//               prefs.remove('user');
//               Future.delayed(Duration(seconds: 2)).then((value) {
//                 Navigator.pushReplacement(
//                   context,
//                   MaterialPageRoute(builder: (context) =>  LogingScreen()),
//                 );
//               });
//             },
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget counter(context){
//     return  //counter
//       Container(
//         padding: EdgeInsets.only(left: 20,right: 25,bottom: 10,top: 10),
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: [
//             //day
//             Container(
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme().bigtittle('2',
//                   ShoopeesTheme.primaryColorDark,FontWeight.w600),
//             ),
//
//             Container(
//               margin: EdgeInsets.only(left: 5),
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme.headline2('Day',
//                   ShoopeesTheme.primaryColorDark,FontWeight.normal),
//             ),
//
//
//             //hour
//             Container(
//               margin: EdgeInsets.only(left: 10),
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme().bigtittle('5',
//                   ShoopeesTheme.primaryColorDark,FontWeight.w600),
//             ),
//
//             Container(
//               margin: EdgeInsets.only(left: 5),
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme.headline2('Hour',
//                   ShoopeesTheme.primaryColorDark,FontWeight.normal),
//             ),
//
//
//             //minute
//             Container(
//               margin: EdgeInsets.only(left: 10),
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme().bigtittle('3',
//                   ShoopeesTheme.primaryColorDark,FontWeight.w600),
//             ),
//
//             Container(
//               margin: EdgeInsets.only(left: 5),
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme.headline2('Min',
//                   ShoopeesTheme.primaryColorDark,FontWeight.normal),
//             ),
//
//
//             //minute
//             Container(
//               margin: EdgeInsets.only(left: 10),
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme().bigtittle('${second.toString()}',
//                   ShoopeesTheme.primaryColorDark,FontWeight.w600),
//             ),
//
//             Container(
//               margin: EdgeInsets.only(left: 5),
//               alignment: Alignment.centerLeft,
//               child: ShoopeesTheme.headline2('Sec',
//                   ShoopeesTheme.primaryColorDark,FontWeight.normal),
//             ),
//           ],
//         ),
//       );
//   }
//
//   Widget Rafer(width){
//     return InkWell(
//       onTap: (){
//         Navigator.push(
//           context,
//           MaterialPageRoute(builder: (context) =>  RefferScreen()),
//         );
//       },
//       child: Card(
//         color:  ShoopeesTheme.primaryColor,
//         elevation: 10,
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(15.0),
//         ),
//         child: Container(
//           // width: width*0.5,
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               SizedBox(
//                 height: 25,
//               ),
//               Image.asset('assets/rafer.png',),
//               SizedBox(
//                 height: 12,
//               ),
//               ShoopeesTheme.headline1('Refer your\n friends',Colors.white,
//                   FontWeight.w700),
//               SizedBox(
//                 height: 25,
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//
//   Widget Grab(width){
//     return InkWell(
//       onTap: (){
//         Navigator.push(
//           context,
//           MaterialPageRoute(builder: (context) =>  GrabIntro()),
//         );
//       },
//       child: Card(
//         color:  ShoopeesTheme.primaryColor,
//         elevation: 10,
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(15.0),
//         ),
//         child: Container(
//           padding: EdgeInsets.all(10),
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               SizedBox(
//                 height: 25,
//               ),
//               Image.asset('assets/reward.png',),
//               SizedBox(
//                 height: 10,
//               ),
//               ShoopeesTheme.headline1('Grab your\n reward', Colors.white,
//                   FontWeight.w700),
//               SizedBox(
//                 height: 25,
//               ),
//
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//
//   Widget footer(width){
//     return Container(
//       child: Column(
//         children: [
//           SizedBox(
//             height: 25,
//           ),
//           ShoopeesTheme.headline2('Get notified when we go live',
//               ShoopeesTheme.primaryColorDark,FontWeight.normal),
//
//           SizedBox(
//             height: 10,
//           ),
//
//           // //subscribe box
//           Container(
//             height: 35,
//             margin: EdgeInsets.only(left: 25,right: 25),
//             width: width,
//             decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(
//                   Radius.circular(10) //
//               ),
//               border: Border.all(
//                 color: ShoopeesTheme.primaryColorDark, //
//                 width: 2.0,
//               ),
//             ),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Container(
//                   padding: EdgeInsets.only(top: 3,left: 15,bottom: 3),
//                   child: ShoopeesTheme.headline3('Enter your email',
//                       ShoopeesTheme.primaryColorDark,FontWeight.w700),
//                 ),
//
//
//
//                 Container(
//                   height: 36.8,
//                   width: 101.5,
//                   decoration: BoxDecoration(
//                     color: ShoopeesTheme.primaryColorDark,
//                     borderRadius: BorderRadius.all(
//                         Radius.circular(8) //
//                     ),
//                   ),
//                   child:Center(
//                     child: ShoopeesTheme.headline3('Subscribe',
//                         Colors.white,FontWeight.w700),
//                   ),
//
//                 )
//               ],
//             ),
//           ),
//
//           //social media
//           Container(
//             margin: EdgeInsets.only(top: 10,left: 75,right: 75),
//             width: width,
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: [
//                 Container(
//                   child: Image.asset('assets/socialmedia/facebook.png'),
//                   width: 25,
//                   height: 25,
//                 ),
//
//                 Container(
//                   child: Image.asset('assets/socialmedia/instagram.png'),
//                   width: 25,
//                   height: 25,
//                 ),
//
//                 Container(
//                   child: Image.asset('assets/socialmedia/twitter-sign.png'),
//                   width: 25,
//                   height: 25,
//                 ),
//
//                 Container(
//                   child: Image.asset('assets/socialmedia/youtube.png'),
//                   width: 25,
//                   height: 25,
//                 ),
//               ],
//             ),
//           ),
//
//           SizedBox(
//             height: 10,
//           ),
//           //text
//           Container(
//             child: ShoopeesTheme.headline2('If you have any question please contact us at',
//                 ShoopeesTheme.primaryColorDark,FontWeight.bold),
//           ),
//           SizedBox(
//             height: 8,
//           ),
//           //text
//           Center(
//             child: ShoopeesTheme.headline2('support@eshopeees.com',
//                 ShoopeesTheme.primaryColorDark,FontWeight.normal),
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget rainbow(width){
//     return Container(
//       width: width,
//       child: Column(
//         children: [
//           Container(
//             width: width,
//             height: 10,
//             decoration: new BoxDecoration(
//               color: ShoopeesTheme.primaryColorDark,
//               borderRadius: BorderRadius.vertical(
//                   bottom: Radius.elliptical(
//                       MediaQuery.of(context).size.width, 100.0)),
//             ),
//           ),
//           Container(
//             width: width,
//             height: 10,
//             decoration: new BoxDecoration(
//               color: ShoopeesTheme.primaryColorLight,
//               borderRadius: BorderRadius.vertical(
//                   bottom: Radius.elliptical(
//                       MediaQuery.of(context).size.width, 100.0)),
//             ),
//           ),
//           Container(
//            width: width,
//            height: 10,
//             decoration: new BoxDecoration(
//               color: ShoopeesTheme.primaryColor,
//               borderRadius: BorderRadius.vertical(
//                   bottom: Radius.elliptical(
//                       MediaQuery.of(context).size.width, 100.0)),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     var width=MediaQuery.of(context).size.width;
//     var height=MediaQuery.of(context).size.height;
//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: appBar(context),
//       drawer: drawer(context),
//       body: Container(
//         padding: EdgeInsets.all(15),
//         width: width,
//         height: height,
//         child: SingleChildScrollView(
//           child: Column(
//             children: [
//               //text
//               SizedBox(
//                 height: 15,
//               ),
//
//               Container(
//                 width: width,
//                 child: Text(
//                   'Welcome to the\nworld of',
//                   textAlign: TextAlign.left,
//                   style: GoogleFonts.poppins(
//                     textStyle: TextStyle(
//                       color: ShoopeesTheme.primaryColorDark,
//                         fontSize: 25,
//                         letterSpacing: .5,
//                         fontWeight:FontWeight.bold,),
//                   ),
//                 ),
//               ),
//
//
//
//               Container(
//                 alignment: Alignment.center,
//                   width: width,
//                   height: 100,
//                   child: Image.asset('assets/logo/logo2.png',)
//               ),
//
//               //counter
//                counter(context),
//               SizedBox(
//                 height: 10,
//               ),
//               // //grab and reffer
//               Row(
//                 children: [
//                   Flexible(
//                     flex: 1,
//                     child: Rafer(width),
//                   ),
//                   Flexible(
//                     flex: 1,
//                     child:  Grab(width),
//                   ),
//
//                 ],
//               ),
//
//               //footer
//               footer(width),
//               //rainbow(width)
//
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

//=============== LogingScreen===============//
//
// class LogingScreen extends StatefulWidget{
//   @override
//   State<LogingScreen> createState() => _LogingScreenState();
// }
//
// class _LogingScreenState extends State<LogingScreen> {
//   TextEditingController ControllerEmail=TextEditingController();
//
//   TextEditingController ControllerPassword=TextEditingController();
//
//   bool showPass=true;
//
//   @override
//   Widget build(BuildContext context) {
//     var width= MediaQuery.of(context).size.width;
//     return Scaffold(
//         resizeToAvoidBottomInset: false,
//         backgroundColor:Colors.white,
//         appBar: AppBar(
//           leading: IconButton(
//             onPressed: (){
//               Navigator.pop(context);
//             },
//             icon: Icon(Icons.arrow_back_ios_rounded,color: Colors.black,),
//           ),
//           elevation: 0,
//           backgroundColor: Colors.white,
//           title: ShoopeesTheme.headline1('Sign into your account',Colors.black,FontWeight.w500),
//         ),
//         body:Container(
//           margin: EdgeInsets.only(top: 30),
//           padding: EdgeInsets.all(15),
//           child: Container(
//             width:width,
//             //height: 450,
//             decoration: BoxDecoration(
//                 color:ShoopeesTheme.lightlogingBg,
//                 borderRadius: BorderRadius.circular(20)
//             ),
//
//             child: Container(
//               padding: EdgeInsets.all(20),
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   //logo
//                   Image.asset('assets/logo/logo2.png', ),
//
//
//                   //email
//                   Container(
//                     padding: EdgeInsets.only(left: 10,right: 10),
//                     child: TextField(
//                       controller: ControllerEmail,
//                       textInputAction: TextInputAction.next,
//                       autocorrect: true,
//                       style: ShoopeesTheme.hint,
//                       keyboardType: TextInputType.emailAddress,
//                       decoration: InputDecoration(
//                           prefixIcon: Icon(Icons.email_outlined),
//                           hintStyle: ShoopeesTheme.hint,
//                           hintText: 'Email'
//                       ),
//                     ),
//                   ),
//
//                   //pass
//                   Container(
//                     padding: EdgeInsets.only(left: 10,right: 10,top: 10),
//                     child: TextField(
//                       controller: ControllerPassword,
//                       textInputAction: TextInputAction.next,
//                       autocorrect: true,
//                       style: ShoopeesTheme.hint,
//                       keyboardType: TextInputType.visiblePassword,
//                       obscureText: showPass,
//                       decoration: InputDecoration(
//                         suffix: InkWell(
//                             onTap: (){
//                               setState(() {
//                                 showPass==false?showPass=true:showPass=false;
//                               });
//                             },
//                             child: Icon(showPass==true?Icons.visibility_off_outlined:Icons.visibility_outlined)),
//                         prefixIcon: Icon(Icons.lock_outline_rounded),
//                         hintStyle: ShoopeesTheme.hint,
//                         hintText: 'Password',
//                       ),
//                     ),
//                   ),
//
//                   //button
//                   Container(
//                     margin: EdgeInsets.only(top: 45,left: 15,right: 15),
//                     child: InkWell(
//                       onTap: (){
//                         String email=ControllerEmail.text;
//                         String pass=ControllerPassword.text;
//                         if(email.isEmpty){
//                           ShoopeesTheme.showToasty(text: 'Enter your email');
//
//                         }else if(pass.isEmpty){
//                           ShoopeesTheme.showToasty(text: 'Enter your password');
//                         }else{
//
//                           loginrepo(
//                               email:ControllerEmail.text,
//                               pass: ControllerPassword.text).then((value){
//                             print('valuevaluevalue==$value');
//                             if(value=='Succsess'){
//                               Navigator.push(
//                                 context,
//                                 MaterialPageRoute(builder: (context) =>  MainScreen()),
//                               );
//                             }else{
//
//                             }
//                           });
//                         }
//
//
//                       },
//                       child: Card(
//                         shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(10),
//                         ),
//                         color: ShoopeesTheme.primaryColor,
//                         elevation: 0,
//                         child: Container(
//                           width: width,
//                           padding: EdgeInsets.all(15),
//                           child:Center(child: ShoopeesTheme.headline1('Sign in',Colors.white,FontWeight.w500)),
//                         ),
//                       ),
//                     ),
//                   ),
//                   Container(
//                     alignment: Alignment.center,
//                     margin: EdgeInsets.only(top: 20,left: 20,right: 20,),
//                     width: width,
//                     child:ShoopeesTheme.headline2("Forgot password?  ",Colors.black,FontWeight.normal) ,
//                   ),
//                   //text
//
//                   Container(
//                     margin: EdgeInsets.only(top: 20,left: 20,right: 20,),
//                     width: width,
//                     child:Row(
//                       children: [
//                         ShoopeesTheme.headline2("Don't have an account?  ",Colors.black,FontWeight.normal),
//
//                         InkWell(
//                             onTap: (){
//                               print('dasfdsafadsfsdf');
//                               Navigator.push(
//                                 context,
//                                 MaterialPageRoute(builder: (context) =>  RegisterScreen()),
//                               );
//                             },
//                             child: ShoopeesTheme.headline2("Sign up",ShoopeesTheme.primaryColor,FontWeight.normal)
//                         )
//                       ],
//                     ) ,
//                   )
//
//
//                 ],
//               ),
//             ),
//           ),
//         )
//     );
//   }
// }



//=========================Register=======================//
// class RegisterScreen extends StatefulWidget{
//   @override
//   State<RegisterScreen> createState() => _RegisterScreenState();
// }
//
// class _RegisterScreenState extends State<RegisterScreen> {
//   TextEditingController Controller_name=TextEditingController();
//
//   TextEditingController Controller_number=TextEditingController();
//
//   TextEditingController Controller_email=TextEditingController();
//
//   TextEditingController Controller_password=TextEditingController();
//
//   String name;
//   String number;
//   String email;
//   String password;
//   String token ;
//   String _verificationCode;
//   String  CountryCode='+91';
//   bool showPass=true;
//
//   FirebaseAuth auth = FirebaseAuth.instance;
//   ValueNotifier<int> timeOut=ValueNotifier(60);
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//
//     getDiviceTokenId();
//
//
//
//   }
//
//   getDiviceTokenId() async {
//     FirebaseMessaging masseging=FirebaseMessaging();
//     token = await masseging.getToken();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       resizeToAvoidBottomInset: false,
//       backgroundColor:Colors.white,
//       appBar: AppBar(
//         leading: IconButton(
//           onPressed: (){
//             Navigator.pop(context);
//           },
//           icon: Icon(Icons.arrow_back_ios_rounded,color: Colors.black,),
//         ),
//         elevation: 0,
//         backgroundColor: Colors.white,
//         title: ShoopeesTheme.headline1('Sign into your account',Colors.black,FontWeight.w500),
//       ),
//       body: Container(
//         padding: EdgeInsets.all(20),
//         child: Stack(
//           alignment: Alignment.center,
//           children: [
//             Container(
//               padding: EdgeInsets.all(20),
//               decoration: BoxDecoration(
//                   color:ShoopeesTheme.lightlogingBg,
//                   borderRadius: BorderRadius.circular(15)
//               ),
//               child: ListView(
//                 //shrinkWrap: true,
//                 children: [
//                   //logo
//                   Image.asset('assets/logo/logo2.png',height: 100,),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   //name
//                   Container(
//                     padding: EdgeInsets.only(left: 10,right: 10),
//                     child: TextField(
//                       controller:Controller_name ,
//                       textInputAction: TextInputAction.next,
//                       autocorrect: true,
//                       style: ShoopeesTheme.hint,
//                       keyboardType: TextInputType.name,
//                       decoration: InputDecoration(
//                           prefixIcon: Icon(Icons.person_outline_outlined),
//                           hintStyle: ShoopeesTheme.hint,
//                           hintText: 'Full name'
//                       ),
//                     ),
//                   ),
//
//
//
//                   Row(
//                     children: [
//                       Flexible(
//                         flex:2,
//                         child: CountryPickerDropdown(
//                           selectedItemBuilder: (country){
//                             return Center(
//                               child: ShoopeesTheme.headline3('+${country.phoneCode}',
//                                   ShoopeesTheme.primaryColorDark,
//                                   FontWeight.normal),
//                             );
//                           },
//
//                           initialValue: 'IN',
//                           itemBuilder: _buildDropdownItem,
//                           dropdownColor: Colors.white,
//                           //itemFilter:  ['AR', 'DE', 'GB', 'CN'],
//                           priorityList:[
//                             CountryPickerUtils.getCountryByIsoCode('GB'),
//                             CountryPickerUtils.getCountryByIsoCode('CN'),
//                           ],
//                           sortComparator: (Country a, Country b) => a.isoCode.compareTo(b.isoCode),
//                           onValuePicked: (Country country) {
//                             print("${country.name}");
//                             CountryCode=country.phoneCode.toString();
//                           },
//
//                         ),
//                       ),
//
//
//                       Flexible(
//                         flex: 6,
//                         child: Container(
//                           padding: EdgeInsets.only(left: 10,right: 10,top: 10),
//                           child: TextField(
//                             controller:Controller_number ,
//                             textInputAction: TextInputAction.next,
//                             autocorrect: true,
//                             style: ShoopeesTheme.hint,
//                             keyboardType: TextInputType.number,
//                             maxLength: 10,
//                             decoration: InputDecoration(
//                                 prefixIcon: Icon(Icons.phone_iphone_rounded ),
//                                 hintStyle: ShoopeesTheme.hint,
//                                 hintText: 'Mobile number'
//                             ),
//                           ),
//                         ),
//                       )
//
//                     ],
//                   ),
//
//
//                   //email
//                   Container(
//                     padding: EdgeInsets.only(left: 10,right: 10,top: 10),
//                     child: TextField(
//                       controller:Controller_email ,
//                       textInputAction: TextInputAction.next,
//                       autocorrect: true,
//                       style: ShoopeesTheme.hint,
//                       keyboardType: TextInputType.emailAddress,
//                       decoration: InputDecoration(
//                           prefixIcon: Icon(Icons.email_outlined),
//                           hintStyle: ShoopeesTheme.hint,
//                           hintText: 'Email'
//                       ),
//                     ),
//                   ),
//
//                   //pass
//                   Container(
//                     padding: EdgeInsets.only(left: 10,right: 10,top: 10),
//                     child: TextField(
//                       controller:Controller_password ,
//                       textInputAction: TextInputAction.next,
//                       autocorrect: true,
//                       style: ShoopeesTheme.hint,
//                       keyboardType: TextInputType.visiblePassword,
//                       obscureText: showPass,
//                       decoration: InputDecoration(
//                         suffix: InkWell(
//                             onTap: (){
//                               setState(() {
//                                 showPass==false?showPass=true:showPass=false;
//                               });
//                             },
//                             child: Icon(showPass==true?Icons.visibility_off_outlined:Icons.visibility_outlined)),
//                         prefixIcon: Icon(Icons.lock_outline_rounded),
//                         hintStyle: ShoopeesTheme.hint,
//                         hintText: 'Password',
//                       ),
//                     ),
//                   ),
//
//                   //button
//                   Container(
//                     margin: EdgeInsets.only(top: 45,left: 15,right: 15),
//                     child: InkWell(
//                       onTap: (){
//                         validation(context);
//                       },
//                       child: Card(
//                         shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(10),
//                         ),
//                         color: ShoopeesTheme.primaryColor,
//                         elevation: 0,
//                         child: Container(
//                           padding: EdgeInsets.all(15),
//                           child:Center(child: ShoopeesTheme.headline1('Sign up',
//                               Colors.white,FontWeight.w500)),
//                         ),
//                       ),
//                     ),
//                   ),
//
//                   //text
//
//                   Container(
//                     margin: EdgeInsets.only(top: 20,left: 20,right: 20,),
//                     child:Row(
//                       children: [
//                         ShoopeesTheme.headline2("Already have an account?  ",
//                             Colors.black,FontWeight.normal),
//
//                         InkWell(
//                             onTap: (){
//                               Navigator.push(
//                                 context,
//                                 MaterialPageRoute(builder: (context) =>  LogingScreen()),
//                               );
//                             },
//                             child: ShoopeesTheme.headline2("Sign in",ShoopeesTheme.primaryColor,FontWeight.normal)
//                         )
//                       ],
//                     ) ,
//                   )
//
//                 ],
//               ),
//
//             )
//           ],
//         ),
//       ),
//     );
//   }
//
//   showbottomSheet(context){
//     Timer.periodic(Duration(seconds: 1), (Timer t) {
//       if(timeOut.value==0){
//         timeOut.value=120;
//       }else{
//         timeOut.value=timeOut.value-1;
//       }
//     });
//     TextEditingController textEditingController=TextEditingController();
//     return showModalBottomSheet(
//         backgroundColor: Colors.white,
//         context: context,
//         builder: (context) {
//           return Container(
//             padding: EdgeInsets.all(20),
//             child: ListView(
//               children: <Widget>[
//                 Container(
//                   child: ShoopeesTheme.headline1("Verify mobile number",
//                       ShoopeesTheme.primaryColorDark,FontWeight.bold),
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Container(
//                   child: ShoopeesTheme.headline2("Enter your otp number",
//                       ShoopeesTheme.primaryColorDark,FontWeight.normal),
//                 ),
//                 SizedBox(
//                   height: 50,
//                 ),
//                 Container(
//                   padding: EdgeInsets.only(left: 30,right: 30),
//                   child: PinCodeTextField(
//                     appContext: context,
//                     length: 6,
//                     obscureText: false,
//                     animationType: AnimationType.fade,
//                     pinTheme: PinTheme(
//                       shape: PinCodeFieldShape.box,
//                       borderRadius: BorderRadius.circular(5),
//                       fieldHeight: 50,
//                       fieldWidth: 40,
//                       selectedFillColor: Colors.white,
//                       inactiveFillColor: ShoopeesTheme.gradientprimaryColor2,
//                       activeColor: Colors.white,
//                       inactiveColor:Colors.white ,
//                       selectedColor: Colors.white,
//                       disabledColor: ShoopeesTheme.primaryColorDark,
//                       activeFillColor: Colors.white,
//                     ),
//                     animationDuration: Duration(milliseconds: 300),
//
//                     enableActiveFill: true,
//                     controller: textEditingController,
//                     onCompleted: (v) {
//                       print("Completed");
//                     },
//                     onChanged: (value) {
//                       print(value);
//
//                     },
//                     beforeTextPaste: (text) {
//                       print("Allowing to paste $text");
//                       return true;
//                     },
//                   ),
//                 ),
//                 SizedBox(
//                   height: 5,
//                 ),
//                 ValueListenableBuilder<int>(
//                   valueListenable: timeOut,
//                   builder: (BuildContext context, int value, Widget child){
//                     return Container(
//                       child: ShoopeesTheme.headline2("We will sed OTP within ${value.toString()} seconds",
//                           ShoopeesTheme.primaryColorDark,FontWeight.normal),
//                     );
//                   },
//                 ),
//
//                 //button
//                 Container(
//                   margin: EdgeInsets.only(top: 45,left: 15,right: 15),
//                   child: InkWell(
//                     onTap: (){
//                       onVeriFy(otp: textEditingController.text);
//                     },
//                     child: Card(
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(10),
//                       ),
//                       color: ShoopeesTheme.primaryColor,
//                       elevation: 0,
//                       child: Container(
//                         padding: EdgeInsets.all(15),
//                         child:Center(child: ShoopeesTheme.headline1('Verify',
//                             Colors.white,FontWeight.w500)),
//                       ),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           );
//         });
//   }
//
//   Future<void> validation(context) async {
//     name=Controller_name.text;
//     number=Controller_number.text;
//     email=Controller_email.text;
//     password=Controller_password.text;
//     if(name.isEmpty){
//       ShoopeesTheme.showToasty(text: 'Enter your name');
//
//     }else if(number.isEmpty){
//       ShoopeesTheme.showToasty(text: 'Enter your mobile number');
//
//     }else if(email.isEmpty){
//       ShoopeesTheme.showToasty(text: 'Enter your email address');
//
//     }else if(password.isEmpty){
//       ShoopeesTheme.showToasty(text: 'Enter your password');
//
//     }else{
//       showbottomSheet(context);
//       sendOTP();
//     }
//
//     // showbottomSheet(context);
//
//   }
//
//   Future<void> sendOTP() async {
//     var num=CountryCode+number;
//     print('number===$num');
//     await FirebaseAuth.instance.verifyPhoneNumber(
//         phoneNumber: num,
//         verificationCompleted: (PhoneAuthCredential credential) async {
//           await FirebaseAuth.instance
//               .signInWithCredential(credential)
//               .then((value) async {
//             if (value.user != null) {
//               print(value.user);
//
//             }
//           });
//         },
//         verificationFailed: (FirebaseAuthException e) {
//
//           log('Regis=Error${e.message.toString()}');
//           ShoopeesTheme.showToasty(text: 'Check your internet connection');
//         },
//         codeSent: ( verficationID,  resendToken) {
//           print(verficationID);
//           _verificationCode=verficationID;
//           ShoopeesTheme.showToasty(text: 'OTP Sented');
//
//         },
//         codeAutoRetrievalTimeout: (String verificationID) {
//           print('codeAutoRetrievalTimeout');
//           ShoopeesTheme.showToasty(text: 'Timed out waiting for OTP');
//           _verificationCode=verificationID;
//
//         },
//         timeout: Duration(seconds: 120));
//   }
//
//   Future<void> onVeriFy({@required otp}) async {
//     try {
//       log('otpptptpt$otp');
//       await FirebaseAuth.instance
//           .signInWithCredential(PhoneAuthProvider.credential(
//           verificationId: _verificationCode, smsCode: otp))
//           .then((value) async {
//         if (value.user != null) {
//           String uid=value.user.uid;
//           registerUser(
//               name: name,
//               number: number,
//               email: email,
//               password: password,
//               token:token,
//               id: uid.toString(),
//               verifiedPhone:true,
//               verificationId:_verificationCode
//           ).then((value) {
//             if(value=='Success'){
//               FirebaseFirestore db=FirebaseFirestore.instance;
//               FirebaseAuth auth=FirebaseAuth.instance;
//               var uid=auth.currentUser?.uid;
//               db.collection('Refferr').doc(uid).set(
//                   {
//                     'uid':uid,
//                     'reffered':0
//                   }
//               ).then((value) => {
//               });
//
//               Navigator.pushAndRemoveUntil(
//                   context,
//                   MaterialPageRoute(builder: (context) => LogingScreen()),
//                       (route) => false);
//             }
//           });
//
//
//         }
//       });
//     } catch (e) {
//       log('otpptptpt$e');
//       ShoopeesTheme.showToasty(text: 'invalid OTP');
//
//     }
//   }
//
//   Widget _buildDropdownItem(Country country) => Container(
//     child: Container(
//       child: Row(
//         children: <Widget>[
//           //CountryPickerUtils.getDefaultFlagImage(country),
//           SizedBox(
//             width: 8.0,
//           ),
//           Text("+${country.phoneCode}"),
//         ],
//       ),
//     ),
//   );
// }